import React, { useEffect, useState } from 'react';
import TodoForm from './TodoForm';
import Todo from './Todo';
import axios from 'axios';
import {useCookies} from 'react-cookie';
import '../styles/TodoList.css';

const TodoList = props => {

    const [todos, setTodos] = useState(props.todos);

    const [cookie, setCookie, removeCookie] = useCookies(['auth_token']);

    const create = newTask => {
        
        const newTodo = {task: newTask, completed: false};
        
        const params = `?task=${newTodo.task}&completed=${newTodo.completed}`;
        axios.post(`http://localhost:8080/todo${params}`, null, {
            headers: {
                Authorization: `Bearer ${cookie["auth_token"]}`
            }
        }).then(res => {
            setTodos([...todos, {...newTodo, id: res.data}]);
        }).catch(err => {
            console.log(err);
        });
    }

    const remove = id => {
        setTodos(todos.filter(todo => todo.id !== id));
        axios.delete(`http://localhost:8080/todo/${id}`, {
            headers: {
                Authorization: `Bearer ${cookie["auth_token"]}`
            }
        }).then(res => {
        }).catch(err => {
            console.log(err);
        });
    };

    const update = (id, updatedTask) => {
        setTodos(todos.map(todo => {
            if (todo.id === id) return {...todo, task: updatedTask};
            return todo;
        }));
        const params = `?task=${updatedTask}`;
        axios.put(`http://localhost:8080/updateTodo/${id}/task${params}`, null, {
            headers: {
                Authorization: `Bearer ${cookie["auth_token"]}`
            }
        });
    };

    const toggleCompletion = id => {
        const completed = todos.find(todo => todo.id === id).completed;
        setTodos(todos.map(todo => {
            if (todo.id === id) return {...todo, completed: !completed};
            return todo;
        }));
        const params = `?completed=${!completed}`;
        axios.put(`http://localhost:8080/updateTodo/${id}/updateStatus${params}`, null, {
            headers: {
                Authorization: `Bearer ${cookie["auth_token"]}`
            }
        });
    };

    return (
        <div className="TodoList">
            <ul>
                {todos.map(todo => {
                    return (
                        <Todo 
                            key={todo.id} 
                            id={todo.id} 
                            task={todo.task}
                            completed={todo.completed} 
                            removeTodo={remove}
                            updateTodo={update}
                            toggleTodo={toggleCompletion}
                        />
                    );
                })}
            </ul>
            <TodoForm createTodo={create}/>
        </div>
    );
}
export default TodoList;