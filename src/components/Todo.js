import React, { useState } from 'react';
import '../styles/Todo.css';
import Checkbox from './Checkbox';
import TaskInput from './TaskInput';
import useFormState from '../hooks/useFormState';

const Todo = props => {

    const [isEditing, setIsEditing] = useState(false);
    const [task, updateTask, resetTask] = useFormState(props.task);

    const handleRemove = () => {
        props.removeTodo(props.id);
    };

    const handleUpdate = () => {
        props.updateTodo(props.id, task);
        setIsEditing(false);
    };

    const handleToggle = () => {
        props.toggleTodo(props.id);
    }

    let display;
    if (isEditing) {
        display = (
            <TaskInput 
                value={task} 
                onChange={updateTask} 
                onConfirm={handleUpdate}  
                onCancel={e => setIsEditing(false)}
                confirmLabel='Save'
            />
        );
    } else {
        display = (
            <div className="Todo">
                <div className="checkbox-col" onClick={handleToggle}>
                    <Checkbox addStyleClass="Todo-check" checked={props.completed} />
                </div>
                <li className={`Todo-task ${props.completed && "completed"}`} onClick={handleToggle}>
                    {task}
                </li>
                <div className="Todo-buttons">
                    <button onClick={e => setIsEditing(true)}>
                        <i className="fas fa-edit"></i>
                    </button>
                    <button onClick={handleRemove}>
                        <i className="fas fa-trash"></i>
                    </button>
                </div>
            </div>
        );
    }

    return display;
}
export default Todo;