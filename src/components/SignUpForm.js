import '../styles/SignForm.css';
import useFormState from '../hooks/useFormState';
import axios from 'axios';
import { useEffect } from 'react';
import {useCookies} from 'react-cookie';

const SignUpForm = ({forward, innerSetLoading}) => {

    const [username, updateUsername, resetUsername] = useFormState("");
    const [firstname, updateFirstname, resetFirstname] = useFormState("");
    const [lastname, updateLastname, resetLastname] = useFormState("");
    const [email, updateEmail, resetEmail] = useFormState("");
    const [password, updatePassword, resetPassword] = useFormState("");

    const [cookie, setCookie, removeCookie] = useCookies(['auth_token']);

    const login = e => {
        e.preventDefault();
        innerSetLoading(true);
        axios.post("http://localhost:8080/register", null, {
            params: {
                username: username,
                firstname: firstname,
                lastname: lastname,
                email: email,
                password: password
            }
        }).then(res => {
            setCookie("auth_token", res.data.jwt);
            forward();
        }).catch(err => {
            console.log(err);
            resetUsername();
            resetFirstname();
            resetLastname();
            resetEmail();
            resetPassword();
            innerSetLoading(false);
        });
    }

    return (
        <form className="SignForm" onSubmit={login}>
            <div className="input-field-pair">
                <div className="input-field">
                    <label htmlFor="firstname">Firstname</label>
                    <input required autoFocus
                        id="firstname" 
                        name="firstname"
                        value={firstname}
                        type="text" 
                        placeholder="Firstname" 
                        autoComplete="off"
                        spellCheck="false"
                        onChange={updateFirstname}/>
                    <div className="input-field-outline"></div>
                </div>
                <div className="input-field">
                    <label htmlFor="lastname">Lastname</label>
                    <input required
                        id="lastname" 
                        name="lastname"
                        value={lastname}
                        type="text" 
                        placeholder="Lastname" 
                        autoComplete="off"
                        spellCheck="false"
                        onChange={updateLastname}/>
                    <div className="input-field-outline"></div>
                </div>
            </div>
            <div className="input-field">
                <label htmlFor="username">Username</label>
                <input required
                    id="username" 
                    name="username"
                    value={username}
                    type="text" 
                    placeholder="Username" 
                    autoComplete="off"
                    spellCheck="false"
                    onChange={updateUsername}/>
                <div className="input-field-outline"></div>
            </div>
            <div className="input-field">
                <label htmlFor="email">E-Mail</label>
                <input required
                    id="email" 
                    name="email"
                    value={email}
                    type="email" 
                    placeholder="E-Mail" 
                    autoComplete="off"
                    spellCheck="false"
                    onChange={updateEmail}/>
                <div className="input-field-outline"></div>
            </div>
            <div className="input-field">
                <label htmlFor="password">Password</label>
                <input required id="password" name="password" value={password} type="password" placeholder="Password" onChange={updatePassword}/>
                <div className="input-field-outline"></div>
            </div>
            <button className="submit-button" type="submit">Sign Up</button>
        </form>
    );
}
  
export default SignUpForm;