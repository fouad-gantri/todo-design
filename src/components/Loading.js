import React, { useState } from 'react';
import "../styles/Loading.css";

const Loading = (props) => {

    return <div className="Loading"><div class="lds-ripple"><div></div><div></div></div></div>;
}
export default Loading;