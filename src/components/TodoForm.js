import React, { useState } from 'react';
import useFormState from '../hooks/useFormState';
import '../styles/TodoForm.css';
import TaskInput from './TaskInput';

const TodoForm = ({createTodo}) => {

    const [task, updateTask, resetTask] = useFormState("");
    const [showTaskInput, setShowTaskInput] = useState(false);

    const handleSubmit = () => {
        if (task.trim() !== "") {
            createTodo(task);
            resetTask();
        }
    }

    if (showTaskInput) {
        return <TaskInput value={task} onChange={updateTask} onConfirm={handleSubmit} onCancel={() => setShowTaskInput(false)}/>;
    }
    return (
        <div className="TodoInput-addButton" onClick={() => setShowTaskInput(true)}>
            <i className="fas fa-plus"></i>
            <span>Add task</span>
        </div>
    );
}
export default TodoForm;