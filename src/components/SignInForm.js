import '../styles/SignForm.css';
import useFormState from '../hooks/useFormState';
import axios from 'axios';
import { useEffect } from 'react';
import {useCookies} from 'react-cookie';
import {useNavigate, Link} from 'react-router-dom';


const SignInForm = ({forward, innerSetLoading}) => {

    const [username, updateUsername, resetUsername] = useFormState("");
    const [password, updatePassword, resetPassword] = useFormState("");
    
    const [cookie, setCookie, removeCookie] = useCookies(['auth_token']);

    const login = async e => {
        e.preventDefault();
        innerSetLoading(true);
        try {
            const res = await axios.post("http://localhost:8080/authenticate", { 
                username: username,
                password: password
            });
            const jwtToken = res.data.jwt;
            setCookie("auth_token", jwtToken);
            forward();
        } catch (error) {
            console.log(error);
            resetUsername();
            resetPassword();
            innerSetLoading(false);
        }

    }

    return (
        <form className="SignForm" onSubmit={login}>
            <div className="input-field">
                <label htmlFor="username">Username</label>
                <input required autoFocus
                    id="username" 
                    name="username"
                    value={username} 
                    type="text" 
                    placeholder="Username" 
                    autoComplete="off"
                    spellCheck="false"
                    onChange={updateUsername}/>
                <div className="input-field-outline"></div>
            </div>
            <div className="input-field">
                <label htmlFor="password">Password</label>
                <input required id="password" name="password" value={password} type="password" placeholder="Password" onChange={updatePassword}/>
                <div className="input-field-outline"></div>
            </div>
            <button className="submit-button" type="submit">Sign In</button>
        </form>
    );
}
  
export default SignInForm;