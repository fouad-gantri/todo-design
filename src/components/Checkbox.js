import React, { useState } from 'react';
import '../styles/Checkbox.css';

const Checkbox = (props) => {

    return (
        <div className={`Checkbox ${props.checked ? "checked" : ""}`}>
            <i className="fas fa-check"></i>
        </div>
    );
}
export default Checkbox;