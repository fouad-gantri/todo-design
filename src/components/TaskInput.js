import React, { useState } from 'react';
import "../styles/TaskInput.css";

const TaskInput = ({onConfirm, onCancel, onChange, value, confirmLabel = "Add task", cancelLabel = "Cancel"}) => {
    
    const handleConfirm = e => {
        e.preventDefault();
        onConfirm();
    }

    const handleCancel = e => {
        e.preventDefault();
        onCancel();
    }

    return (
        <form className="TaskInput">
            <input 
                autoFocus
                className="TaskInput-field"
                id="task" 
                type="text"
                autoComplete="off" 
                placeholder="e.g., buy some milk"
                name="task"
                value={value}
                onChange={onChange}
            />
            <button className="TaskInput-confirm" type="submit" onClick={handleConfirm}>{confirmLabel}</button>
            <button className="TodoInput-cancel" onClick={handleCancel}>{cancelLabel}</button>
        </form>
    );
}
export default TaskInput;