import React, { useState } from 'react';
import '../styles/TodoForm.css';
import useFormState from '../hooks/useFormState';

const TodoInput = props => {

    const [task, updateTask, resetTask] = useFormState("");
    const [showInput, setShowInput] = useState(false);
    
    if (showInput) {
        return (
            <div className="TodoInput">
                <form className="TodoInput-form">
                    <input 
                        id="task" 
                        type="text"
                        autoComplete="off" 
                        placeholder="e.g., buy some milk"
                        name="task"
                        value={task}
                        onChange={updateTask}
                    />
                    <button className="TodoInput-form-add" type="submit">Add task</button>
                    <button className="TodoInput-form-cancel" onClick={() => setShowInput(false)}>Cancel</button>
                </form>
            </div>
        )
    }
    return (
        <div className="TodoInput">
            <div className="TodoInput-addButton" onClick={() => setShowInput(true)}>
                <i className="fas fa-plus"></i>
                <span>Add task</span>
            </div>
        </div>
    );
}
export default TodoInput;