import './styles/App.css';
import SignIn from './pages/SignIn';
import SignUp from './pages/SignUp';
import { Route, Routes, Navigate } from 'react-router';
import TodoApp from './pages/TodoApp';
import SignOut from './pages/SignOut';
import Test from './pages/Test';

const App = () => {

  //TODO check if user is logged in ...

  return (
    <div className="App">
      <Routes>
        <Route exact path="/" element={<TodoApp/>}/>
        <Route exact path="/signin" element={<SignIn/>}/>
        <Route exact path="/signup" element={<SignUp/>}/>
        <Route exact path="/signout" element={<SignOut/>}/>
        <Route exact path="/test" element={<Test/>}/>
        <Route path="*" element={<TodoApp/>} />
      </Routes>
    </div>
  );
}

export default App;
