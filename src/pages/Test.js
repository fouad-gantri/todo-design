import React, { useState } from 'react';
import TodoInput from '../components/TodoInput';
import TaskInput from '../components/TaskInput';
import useFormState from '../hooks/useFormState';
import Alert from '../components/Alert';

const Test = props => {

    const [task, updateTask, resetTask] = useFormState("");

    return (
        <div style={{margin:"50px"}} className="Test">
            <Alert/>
        </div>
    );
}
export default Test;