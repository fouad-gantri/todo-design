import React, { useEffect, useState } from 'react';
import {useCookies} from 'react-cookie';
import { Navigate } from 'react-router';

const SignOut = () => {
    const [cookie, setCookie, removeCookie] = useCookies(['auth_token']);
    if (cookie["auth_token"]) removeCookie("auth_token");
    return <Navigate to="/signin"/>;
}
export default SignOut;