import { Link, useNavigate } from "react-router-dom";
import SignInForm from "../components/SignInForm";
import '../styles/Sign.css';
import {useCookies} from 'react-cookie';
import { useEffect, useState } from "react";
import Loading from "../components/Loading";
import Alert from "../components/Alert";

const SignIn = () => {

    const navigate = useNavigate();
    const [loading, setLoading] = useState(true);
    const [cookie, setCookie, removeCookie] = useCookies(['auth_token']);

    useEffect(() => {
        if (cookie["auth_token"]) navigate("/");
        setLoading(false);
    }, []);

    const handleForward = () => {
        navigate("/");
    }

    return (loading ? 
      <Loading/>
      :
      <div className="Sign">
        <h1>Sign In.</h1>
        <SignInForm forward={handleForward} innerSetLoading={(bool) => setLoading(bool)}/>
        <p>Don't have an account? <Link to="/signup">Sign up</Link></p>
      </div>
    );
}
  
export default SignIn;