import { Link, useNavigate } from "react-router-dom";
import SignUpForm from "../components/SignUpForm";
import '../styles/Sign.css';
import {useCookies} from 'react-cookie';
import { useEffect, useState } from "react";
import Loading from "../components/Loading";

const SignUp = () => {

    const navigate = useNavigate();

    const [loading, setLoading] = useState(true);
    const [cookie, setCookie, removeCookie] = useCookies(['auth_token']);

    useEffect(() => {
        if (cookie["auth_token"]) navigate("/");
        setLoading(false);
    }, []);

    const handleForward = () => {
        navigate("/");
    }

    return (loading ?
      <Loading/>
      :
      <div className="Sign">
        <h1>Sign Up.</h1>
        <SignUpForm forward={handleForward} innerSetLoading={(bool) => setLoading(bool)}/>
        <p>Already have an account? <Link to="/signin">Sign in</Link></p>
      </div>
    );
}
  
export default SignUp;