import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router';
import {useCookies} from 'react-cookie';
import axios from 'axios';
import '../styles/TodoApp.css';
import TodoList from '../components/TodoList';
import Loading from '../components/Loading';

const TodoApp = (props) => {

    const navigate = useNavigate();
    const [userDetails, setUserDetails] = useState(null);
    const [todos, setTodos] = useState(null);
    const [loading, setLoading] = useState(true);
    

    const [cookie, setCookie, removeCookie] = useCookies(['auth_token']);

    useEffect(() => {
        const reqUser = axios.get("http://localhost:8080/user", {
            headers: {
                'Authorization': `Bearer ${cookie["auth_token"]}`
            }
        });
        const reqUserTodos = axios.get("http://localhost:8080/todosByUser", {
            headers: {
                'Authorization': `Bearer ${cookie["auth_token"]}`
            }
        });

        axios.all([reqUser, reqUserTodos]).then(axios.spread((user, userTodos) => {
            setUserDetails(user.data);
            setTodos(userTodos.data);
            setLoading(false);
        })).catch(err => {
            logout();
        });

    }, []);

    const logout = () => navigate("/signout");

    if (loading) {
        return <Loading/>; 
    } else {
        return (
            <div className="TodoApp">
                <nav>
                    <h1 className='user-display'>{userDetails.firstname}'s Todos</h1>
                    <form className="logout-form" onSubmit={logout}>
                    <button className="logout" type="submit">
                        <i className="fas fa-sign-out-alt"></i>
                    </button>
                    </form>
                </nav>
                <TodoList todos={todos}/>
            </div>
        );
    }
}
export default TodoApp;